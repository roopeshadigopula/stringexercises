package Assignment2;

public class SpaceRemover {
    public String removeSpace(String str) {
        return str.replaceAll("\\s+", " ");
    }
}
