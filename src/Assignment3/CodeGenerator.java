package Assignment3;

public class CodeGenerator{
    public StringBuilder generateCode(String str) {

        StringBuilder sb = new StringBuilder();
        for(int i=0; i<str.length(); i++)
        {
            sb.append(str.charAt(i));
        }

//        Reverse the Input String
        sb.reverse();

//        First Char of rev String should be removed
        sb.deleteCharAt(0);

//        2nd and 3rd letter should be replaced with *
        sb.replace(0,2,"*");

//        Last char should be changed to #
        sb.replace(sb.length()-1,sb.length(),"#");

//        Should be changed to upper case
//        sb.toString().toUpperCase();
        for(int i = 0 ; i < sb.length() ; i++)
        {
            if(sb.charAt(i) >= 97 && sb.charAt(i) <=122)    // check for small case letter ...   ascii code of "a" is 97 ... "z" is 122.
            {
                sb.setCharAt(i, (char)(sb.charAt(i)-32));     //   The character at the specified index is set to  to uppercase
            }
        }

        return sb;
    }
}
