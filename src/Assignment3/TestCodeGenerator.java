package Assignment3;
import java.util.Scanner;

public class TestCodeGenerator {
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        CodeGenerator obj = new CodeGenerator();
        System.out.println(obj.generateCode(str));
    }
}
