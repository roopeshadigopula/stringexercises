package Assignment4;

public class Verifier {
    public boolean verifyPassword(String str) {
        boolean flag = false;

        if (str.charAt(0) >= 'a' && str.charAt(0) >= 'z' || str.charAt(0) >= 'A' && str.charAt(0) >= 'Z')
        {
            if(str.length()>=6)
            {
                if(str.matches(".*[0-9]{1,}.*") && str.matches(".*[@#!]{1,}.*"))
                {
                    flag = true;
                }
            }
        }

        return flag;
    }
}
