package Assignment4;
import java.util.Scanner;

public class TestVerifier {
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        String pwd = sc.nextLine();

        Verifier obj = new Verifier();
        boolean check = obj.verifyPassword(pwd);

        if(check)
            System.out.println("Valid Password");
        else
            System.out.println("Not a Valid Password");
    }
}
